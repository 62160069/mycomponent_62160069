
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> gebProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "ESPRESSO 1", 40, "1.jpg"));
        list.add(new Product(1, "ESPRESSO 2", 45, "1.jpg"));
        list.add(new Product(1, "ESPRESSO 3", 50, "1.jpg"));

        list.add(new Product(1, "CAPPUCCINO 1", 40, "2.jpg"));
        list.add(new Product(1, "CAPPUCCINO 2", 45, "2.jpg"));
        list.add(new Product(1, "CAPPUCCINO 3", 50, "2.jpg"));

        list.add(new Product(1, "LATTE 1", 45, "3.jpg"));
        list.add(new Product(1, "LATTE 2", 50, "3.jpg"));
        list.add(new Product(1, "LATTE 3", 55, "3.jpg"));
        return list;
    }
}
